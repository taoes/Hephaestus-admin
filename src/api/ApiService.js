import axios from 'axios';
//取消请求
let CancelToken = axios.CancelToken;
axios.create({
    timeout: 15000,// 请求超时时间
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    withCredentials: true
})
export default axios;
