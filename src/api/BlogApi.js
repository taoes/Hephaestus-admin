import service from './ApiService';
import {SERVICE_HOST} from './Service.config.js';


/**
 * 新增或修改文章内容
 * @param blog
 * @returns {AxiosPromise}
 */
export function submitBlog(blog) {
    return service({
        url: SERVICE_HOST + "/blog",
        method: 'patch',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data: blog
    });
}

/**
 * 分页或企业文章列表
 * @param key
 * @param pageNum
 * @param pageSize
 * @returns {AxiosPromise}
 */
export function getBlog(key, pageNum, pageSize) {
    return service({
        url: SERVICE_HOST + `/blog?key=${key}&pageNum=${pageNum}&pageSize=${pageSize}`,
        method: 'get',
        dataType: "json",
        contentType: "application/x-www-form-urlencoded;charset=UTF-8"
    })
}

/**
 * 获取文章的内容详情
 * @param blogId
 * @returns {AxiosPromise}
 */
export function getBlogById(blogId) {
    return service({
        url: `${SERVICE_HOST}/blog/${blogId}`,
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8'
    });
}