import React, {Component} from 'react';
import './Home.css'
import {
    Layout, Menu, Icon,
} from 'antd';
import Resourdes from "../Resources";
import KEY from "../ComponseKey";
import {AddBlog} from "../AddBlog/AddBlog";
import BlogManagement from "../BlogManagement/BlogManagement";

const {SubMenu} = Menu;
const {Header, Content, Sider} = Layout;


export default class Home extends Component {

    contentHeight = 0;

    constructor(props) {
        super(props);
        this.state = {selectKey: KEY.BLOG_MANAGEMENT, param: {}}

    }

    selectKeyEvent = (item) => {
        this.setState({selectKey: item.key})
    };

    changeTab = (selectKey, param) => {
        this.setState({selectKey, param});
    };

    render() {
        this.contentHeight = document.body.clientHeight - 120;

        var content = "21312";
        switch (this.state.selectKey) {
            case KEY.RESOURCE:
                content = <Resourdes/>;
                break;
            case KEY.ADD_BLOG:
                content = <AddBlog param={this.state.param} change={this.changeTab}/>;
                break;
            case KEY.BLOG_MANAGEMENT:
                content = <BlogManagement param={this.state.param} change={this.changeTab}/>
                break;
            case '1':
                content = "123";
                break;
            default:
                content = "123124";
                break;
        }
        return (
            <Layout>
                <Header className="header">
                    <h2 style={{color: 'white'}}>Hephaestus 开源博客后台管理系统</h2>
                    <div className="logo"/>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={[this.state.selectKey]}
                        style={{lineHeight: '64px', float: 'right'}}
                    >
                        <Menu.Item key="1">系统配置</Menu.Item>
                        <Menu.Item key="2"><Icon type="close" theme="filled"/>操作日志</Menu.Item>
                        <Menu.Item key="3">退出</Menu.Item>
                    </Menu>
                </Header>
                <Layout>
                    <Sider collapsible={false} collapsedWidth={0} width={200}
                           style={{marginTop: '20px', background: '#fff', height: this.contentHeight}}>
                        <Menu
                            onSelect={this.selectKeyEvent}
                            theme="light"
                            mode="inline"
                            defaultSelectedKeys={['1']}
                            defaultOpenKeys={['sub1']}
                            style={{height: '100%', borderRight: 0}}
                        >
                            <SubMenu key="sub1" title={<span><Icon type="home"/>文章管理</span>}>
                                <Menu.Item key={KEY.ADD_BLOG}>新增文章</Menu.Item>
                                <Menu.Item key={KEY.BLOG_MANAGEMENT}>文章管理</Menu.Item>
                                <Menu.Item key="3">文章分类</Menu.Item>
                                <Menu.Item key="4">文章标签</Menu.Item>
                                <Menu.Item key="5">阅读记录</Menu.Item>
                            </SubMenu>
                            <SubMenu key="sub2" title={<span><Icon type="sound"/>评论管理</span>}>
                                <Menu.Item key="6">评论列表</Menu.Item>
                            </SubMenu>
                            <Menu.Item key={KEY.RESOURCE}><Icon type="file-pdf"/>资源管理</Menu.Item>
                            <Menu.Item key={KEY.SETTING}><Icon type="setting"/>系统配置</Menu.Item>
                            <Menu.Item key={KEY.ABOUT}><Icon type="tags"/>关于</Menu.Item>
                        </Menu>
                    </Sider>
                    <Layout style={{padding: '20px 24px 24px'}}>
                        <Content style={{
                            background: '#fff', padding: 24, margin: 0, height: this.contentHeight,
                        }}
                        >{content}
                        </Content>
                    </Layout>
                </Layout>
            </Layout>
        );
    }


}