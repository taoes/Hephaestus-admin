import React from 'react';
import {Button, Input, message, Modal, Select, notification, Icon} from 'antd';
import './AddBlog.css'
import Editor from 'tui-editor';
import 'codemirror/lib/codemirror.css'; // codemirror
import 'tui-editor/dist/tui-editor.css'; // editor ui
import 'tui-editor/dist/tui-editor-contents.css'; // editor content
import 'highlight.js/styles/github.css';
import 'tui-chart/dist/tui-chart.js';
import 'tui-editor/dist/tui-editor-Editor-all.js';
import marked from 'marked'
import {getBlogById, submitBlog} from "../../api/BlogApi";
import KEY from "../ComponseKey";
import {WEB_HOST} from "../../api/Service.config";


const {Option, OptGroup} = Select;

const buttonMargin = {
    marginRight: 10
};

const openNotification = (type, title, desc) => {
    notification[type]({
        message: title,
        description: desc,
        icon: <Icon type="smile" style={{color: '#108ee9'}}/>,
    });
};

export class AddBlog extends React.Component {

    editor = undefined;

    constructor(props) {
        super(props);
        this.state = {id: '', title: '', content: '> 请输入文章内容', showConfig: false}
    }

    async componentDidMount() {
        // 获取传入的参数
        let param = this.props.param;
        console.log(param)
        if (param.id === undefined || param.id == '') {
            console.error("Blog没有发现参数")
        } else {
            let id = this.props.param.id;
            let title = this.props.param.title;
            this.setState({id: id, title: title})
            // 获取文章信息
            let request = await getBlogById(id);
            if (request.status != "200") {
                message.error("获取文章内容失败,请返回重试!");
                return;
            }
            let data = request.data.contentMd;
            this.setState({content: data})

        }


        this.editor = new Editor({
            el: document.querySelector('#editSection'),
            previewStyle: 'vertical',
            height: '600px',
            initialEditType: 'markdown',
            useCommandShortcut: true,
            initialValue: this.state.content,
            exts: [
                {
                    name: 'chart',
                    minWidth: 100,
                    maxWidth: 600,
                    minHeight: 100,
                    maxHeight: 300
                },
                'scrollSync',
                'colorSyntax',
                'uml',
                'mark',
                'table'
            ]
        });

    }

    submitBlog = async () => {
        let title = this.state.title;
        if (title == undefined || title.trim() == "") {
            message.error("提交失败,标题不能为空!");
            return;
        }
        let value = this.editor.getValue();
        if (value == undefined || value.trim() == "") {
            message.error("提交失败,文章内容不能为空!");
        }
        let blog = {
            id: this.state.id,
            title: title,
            htmlContent: marked(value),
            mkContent: value,
            description: value.substring(100),
            typeId: ["1", "2", "3"],
            keyWords: "技术",
            allowComment: true,
            allowTop: false
        };

        try {
            let request = await submitBlog(blog);
            if (request.status != '200') {
                openNotification('error', "❌ 操作失败,请重试!",`错误消息:${request.data.message}`);
            } else {
                openNotification('success', "👍 操作成功", `恭喜您,您的文章已经更新成功啦✨ \n<a href=${WEB_HOST}/blog/${request.data.id}>点击这里去查看吧</a>`)
            }
        } catch (e) {
            openNotification('error', "❌ 操作失败,请重试!",'网络连接失败,请确认网络状态是否通畅!');
            return
        }


    };

    // 获取标题
    inputChange = (e) => {
        this.setState({title: e.target.value})
    };

    // 获取关键字
    inputBlogKey = (e) => {
        console.log(e);
    };

    // 获取分类信息
    inputGroup = (value) => {
        console.log(value)
    };

    // 显示模态框
    setBlogConfig(isShow) {
        this.setState({showConfig: isShow})
    }

    render() {
        return (

            <div>
                <Input value={this.state.title} size={"large"} onChange={this.inputChange} name="title"
                       placeholder={"请输入文章标题"}/>

                <div id="editSection"/>
                <Modal
                    title="配置博客选项"
                    centered
                    style={{top: 10}}
                    visible={this.state.showConfig}
                    onOk={() => this.setBlogConfig(false)}
                    onCancel={() => this.setBlogConfig(false)}
                    okText={"确定"}
                    cancelText={"取消"}
                >
                    <Select
                        placeholder={"请选择目录"}
                        style={{
                            width: '95%', marginRight: '5%',
                            marginTop: 0
                        }}
                        onChange={this.inputGroup}>
                        <OptGroup label="Manager">
                            <Option value="jack">Jack</Option>
                            <Option value="lucy">Lucy</Option>
                        </OptGroup>
                        <OptGroup label="Engineer">
                            <Option value="Yiminghe">yiminghe</Option>
                        </OptGroup>
                    </Select>
                    <Select
                        placeholder="请输入关键字信息"
                        mode="tags"
                        style={{width: '95%', marginRight: '5%', marginTop: 10}}
                        onChange={() => this.inputBlogKey}
                        tokenSeparators={[',']}
                    />
                </Modal>
                <div style={{float: 'left', marginTop: 20, visibility: this.state.id === '' ? 'hidden' : 'visible'}}>
                    <Button type="default" onClick={() => this.props.change(KEY.BLOG_MANAGEMENT, {})}>返回</Button>
                </div>
                <div style={{float: 'right', marginTop: 20}}>
                    <Button type="primary" onClick={() => this.submitBlog()} style={buttonMargin}>提交</Button>
                    <Button type="primary" onClick={() => this.setBlogConfig(true)} style={buttonMargin}>配置</Button>
                    <Button type="primary" onClick={() => message.error("保存功能暂未实现")} style={buttonMargin}>保存</Button>
                </div>
            </div>
        );
    }

}