import React from 'react';
import {Avatar, Badge, Icon, List, message, Pagination, Skeleton} from "antd";
import {getBlog} from "../../api/BlogApi";
import './style.css'
import {WEB_HOST} from "../../api/Service.config";
import KEY from "../ComponseKey";

const IconText = ({type, text}) => (
    <span style={{marginRight: 20}}>
    <Icon type={type} style={{marginRight: 4}} theme="twoTone"/>
        {text}
  </span>
);


const cardStyle = {
    width: '94%',
    paddingTop: 0,
    paddingLeft: 0
};
export default class BlogManagement extends React.Component {


    constructor(props) {
        super(props);
        this.state = {blogListSize: 7, blogListTotal: 0, blogList: []};
    }


    async componentDidMount() {

        this.getBlogList("", 1, this.state.blogListSize);
    }

    getBlogList = async (key, page, pageSize) => {
        // 同步获取数据
        let request = await getBlog(key, page, pageSize);
        console.error(request)
        if (request.status != "200") {
            console.error("文章列表获取失败")
            message.error("文章列表获取失败,请检查网络");
            return;
        }
        this.setState({blogList: request.data.list, blogListTotal: request.data.total});
    };


    pageChange = (page, pageSize) => {
        console.log(`文章切换到${page} - ${pageSize}`);
        this.getBlogList("", page, pageSize)
    };

    render() {
        return (
            <div className="blogManagement">
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={this.state.blogList}
                    renderItem={item => (
                        <List.Item actions={[<a onClick={() => this.props.change(KEY.ADD_BLOG, {
                            id: item.id,
                            title: item.title
                        })}>编辑</a>, <a href={WEB_HOST + `/blog/${item.id}`} target="_blank">查看</a>]}
                                   style={{marginTop: 20,}}>
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Badge title={"文章阅读量"} count={item.vblogLookCount}
                                               style={{backgroundColor: '#52c41a'}} overflowCount={10000}>
                                            <Avatar
                                                src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>
                                        </Badge>}
                                    title={<span style={{fontSize: 18, color: 'gray'}}
                                                 href="https://ant.design">{item.title}</span>}
                                    description={item.description.substring(0, 200)}/>
                                <div></div>
                            </Skeleton>
                        </List.Item>
                    )}
                />
                < Pagination className="pageInfo" showQuickJumper defaultCurrent={1}
                             defaultPageSize={this.state.blogListSize}
                             total={this.state.blogListTotal}
                             onChange={this.pageChange}/>
            </div>
        );
    }

}