import React, {Component} from 'react';
import store from './store/store';
import {Provider} from 'mobx-react';
import Home from "./componse/Home/Home";
import './App.css'

export default class App extends Component {
  render() {
    return (
      <Provider todolist={store}>
        <Home />
      </Provider>
    );
  }
};
